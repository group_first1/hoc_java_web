package com.nct99.entity;

public class GiamDoc extends Employee{
	String chucVu;

	
	public GiamDoc() {
		super();
	}

	public GiamDoc(String chucVu,String ten,int tuoi) {
		super(ten,tuoi);
		this.chucVu = chucVu;
	}

	public String getChucVu() {
		return chucVu;
	}

	public void setChucVu(String chucVu) {
		this.chucVu = chucVu;
	}
	
}
