package com.nct99.entity;

public class Employee {
	String tenNhanVien;
	String diaChi;
	int tuoi;
	GiamDoc giamDoc;

	public Employee() {
		super();
	}
	
	public Employee(GiamDoc giamDoc) {
		super();
		this.giamDoc = giamDoc;
	}

	public Employee(String tenNhanVien, int tuoi) {
		super();
		this.tenNhanVien = tenNhanVien;
		this.tuoi = tuoi;
	}
	
	public Employee(String tenNhanVien, String diaChi, int tuoi) {
		super();
		this.tenNhanVien = tenNhanVien;
		this.diaChi = diaChi;
		this.tuoi = tuoi;
	}
	
	public GiamDoc getGiamDoc() {
		return giamDoc;
	}

	public void setGiamDoc(GiamDoc giamDoc) {
		this.giamDoc = giamDoc;
	}

	public String getTenNhanVien() {
		return tenNhanVien;
	}
	public void setTenNhanVien(String tenNhanVien) {
		this.tenNhanVien = tenNhanVien;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public int getTuoi() {
		return tuoi;
	}
	public void setTuoi(int tuoi) {
		this.tuoi = tuoi;
	}
	
	public void showMess()
	{
		System.out.println("hello bạn: "+tenNhanVien);
	}
	
	public void start()
	{
		System.out.println("init đối tượng employee");
	}
	
	public void end()
	{
		System.out.println("destroy employee");
	}
}
