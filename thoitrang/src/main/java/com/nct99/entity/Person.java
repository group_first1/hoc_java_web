package com.nct99.entity;

public class Person {
	String name;
	
	public Person() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
		
	public static Person createPerson()
	{
		Person p=new Person();
		p.name="t�n person.";
		return p;
	}
	
	public Person createPerson2()
	{
		Person p=new Person();
		p.name="t�n person 2.";
		return p;
	}
	
	public void noti()
	{
		System.out.println("t�n l�:" +name);
	}
}
